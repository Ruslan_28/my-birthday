import axios from 'axios'

export const GET_TODOS = "GET_TODOS"

export function getTodos() {
    return dispatch => {
      return axios.get('http://test.anromsocial.com/api/birthdays')
          .then(response => response.data)
          .then(todos => dispatch({
              type: GET_TODOS,
              todos
          }))
          .catch(error => console.info('error'))
    }
}
