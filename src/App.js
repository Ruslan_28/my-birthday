import React, { Component } from 'react'
import ContainerUserList from './components/UserList'
import './App.css'

class App extends Component {
  render() {
    return (
      <main>
        <ContainerUserList />
      </main>
    );
  }
}

export default App;
