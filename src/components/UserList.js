import React, { Component } from 'react'
import { connect } from "react-redux"

class UserList extends Component {
    render() {
        return (
            <div>

              {
                this.props.todos.users
                ?
                <div>
                  {this.props.todos.users.map((item, index) => {
                    return (
                      <p key={index}>{item.name}</p>
                    )
                  })}
                </div>
                :
                <p>Download</p>
              }

            </div>
        )
    }
}

const ContainerUserList = connect(
    state => ({
        todos: state.todos
    })
)(UserList)

export default ContainerUserList;
